# Mangatheque API

## INSTALLATION

1. `npm install`  
2. `cp .env.exemple .env`
3. > Fill the .env file with your data
4. `npm run db:migrate` afin de créer toutes les tables en base
5. `npm run dev`


## Architecture du projet
### /app
contient tous les dossiers relatifs au fonctionnement de l'application (classes, scripts...)
 #### /app/Classes
Les classes fonctionnelles de l'application et modèles de données
#### /app/DBScripts
Les différents script permettant de gérer la base (migrer/détruire) via node
#### /app/http
Code relatif a la gestion de requêtes http
##### /app/http/Controllers
Les différents controlleurs, en général un controlleur est lié a un model et a la base de donnée grâce a la classe Connector de ce dernier
##### /app/http/Helpers
Les classes d'aide permettant d'avoir du code (pas forcément du fonctionnel) qui puisse se partager au travers de toute l'application
##### /app/http/Middlewares
Le nom est explicite ce sont les middleware qui s'occuperont d'effectuer certaines tâches selon le contenu des requêtes qui leur passent dedans, comme vérifier la validité des JWT
### /openAPI
Contient un export de la spécification de l'api au format JSON ou Yaml, export effectué via openAPI (swagger)

### .env / .env.exemple
Les variables d'environnement de l'application

### index.js
le code de l'application, on y retrouve la connexion a la base, le setup de tous les modèles et leurs relations conformément au schémas fourni dans le compte-rendu, la création de toutes les routes et le lancement du serveur express