import express from 'express'
import dotenv from 'dotenv'
import bodyParser from "body-parser";
import cors from 'cors'

// Class
import DB from "./app/Classes/DB.js";
import User from "./app/Classes/User.js";
import Manga from "./app/Classes/Manga.js"
import UserController from "./app/http/Controllers/UserController.js";
import AuthController from "./app/http/Controllers/AuthController.js";
import MangaController from "./app/http/Controllers/MangaController.js";
import GenreController from "./app/http/Controllers/GenreController.js";
import ThemeController from "./app/http/Controllers/ThemeController.js";

// Middlewares
import authenticated from "./app/http/Middlewares/authenticated.js";
import Genre from "./app/Classes/Genre.js";
import Theme from "./app/Classes/Theme.js";



dotenv.config()
const SESSION_SECRET = {
    SECRET_EXPIRATION: 3600,
    SECRET_KEY:process.env.SECRET_TOKEN
}

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

const db = new DB({
    host: process.env.HOST,
    port: process.env.DATABASE_PORT,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    multipleStatements: true
})

// Prepare models
User.setConnection(db)
Manga.setConnection(db)
Genre.setConnection(db)
Theme.setConnection(db)

// Prepare relationships
User.setRelationShip('mangas', Manga)
Genre.setRelationShip('mangas', Manga)
Theme.setRelationShip('mangas', Manga)
Manga.setRelationShip('themes', Theme)
Manga.setRelationShip('genres', Genre)


// Prepare controllers
const authController = new AuthController()
const userController = new UserController(User)
const mangaController = new MangaController(Manga)
const genreController = new GenreController(Genre)
const themeController = new ThemeController(Theme)

const routesGenerator = (controller, model, authRoutes = ['update', 'find', 'delete'], exclude = []) => {
    const controllerMethods = {
        'all':{'verb': 'get', params: [], auth: authRoutes.includes('all'), model: false},
        'store':{'verb': 'post', params: [], auth: authRoutes.includes('store'), model: true},
        'update':{'verb': 'patch', params: ['id'], auth: authRoutes.includes('update'), model: true},
        'find':{'verb': 'get', params: ['id'], auth: authRoutes.includes('find'), model: false},
        'delete':{'verb': 'delete', params: ['id'], auth: authRoutes.includes('delete'), model: false}
    }
    const router = express.Router()
    Object.keys(controllerMethods).forEach((method) => {

        if(exclude.includes(method))  return

        let path = controllerMethods[method].params.length ? `/:${controllerMethods[method].params.join('/:')}` : '/'
        let controllerCb
        if( controllerMethods[method].model){
            controllerCb = (req, res, model) => controller[method](req, res, model)
        }else{
            controllerCb = (req, res) => controller[method](req, res)
        }
        if(controllerMethods[method].auth){
            router[controllerMethods[method].verb](path, authenticated, (req, res) => controllerCb(req, res, model))
        }else{
            router[controllerMethods[method].verb](path, (req, res) => controllerCb(req, res, model))
        }
    })

    if(Object.keys(model?.relationships ?? {}).length){
        Object.keys(model.relationships).forEach(relation => {
            if(controller?.model?.[relation]){
                router.get(`/:id/relations/${relation}`, ({params, body}, res) => controller.model[relation]({params, body}, res))
            }
            if(controller?.model?.attach){
                router.post(`/:id/relations/:relation`,authenticated, ({params, body}, res) => controller.model.attach({params, body}, res))
            }
            if (controller?.model?.detach){
                router.delete(`/:id/relations/:relation`,authenticated, ({params, body}, res) => controller.model.detach({params, body}, res))
            }
        })
    }

    return router

}



app.listen(process.env.PORT, function () {
    console.log(`Ready on port ${process.env.PORT}`)
})

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Request-Headers", "*")
    next();
});

app.get('/', function (req, res) {
    res.status(200).send('Mangatheque API is up.')
})


// Auth routes -----------------------------------------------------------------------------------------------
const authRoutes = express.Router();
authRoutes.post('/login', (req, res) => authController.login(req, res, User, SESSION_SECRET))
authRoutes.get('/logout', authenticated, (req, res) => authController.logout(req, res, User))

// Users routes -----------------------------------------------------------------------------------------------
const usersRoutes = routesGenerator(userController, User, ['find', 'update', 'delete']);

// Mangas routes -----------------------------------------------------------------------------------------------
const mangasRoutes = routesGenerator(mangaController, Manga, [], ['delete', 'update']);
mangasRoutes.get('/exists/:id', (req, res) => mangaController.exists(req, res))

// Themes routes
const themesRoutes = routesGenerator(themeController, Theme, ['delete'], ['update']);

// Genres routes
const genresRoutes = routesGenerator(genreController, Genre, ['delete'], ['update']);

// Feed routes
const feedRouter = express.Router()
feedRouter.get('/random', (req, res) => mangaController.random(req, res))
feedRouter.get('/discover', (req, res) => mangaController.discover(req, res))
feedRouter.get('/top', (req, res) => mangaController.top(req, res))

usersRoutes.post('/:user_id/relations/mangas/:manga_id', authenticated, (req, res) => userController.ownVolume(req, res, Manga))

// Attach all defined routes to app
app.use('/', authRoutes)
app.use('/users', usersRoutes)
app.use('/mangas', mangasRoutes)
app.use('/themes', themesRoutes)
app.use('/genres', genresRoutes)
app.use('/feed', feedRouter)

