export default class HttpException{
    #codes = {
        400: 'Bad request',
        401: 'Unauthorized',
        403: 'Forbidden',
        404: 'Not found',
        419: 'Session expired',
        422: 'Unprocessable entity',
        429: 'Too many requests',
        500: 'Internal server error',
        502: 'Bad Gateway'
    }
    constructor(code, description, res, complement = {}) {
        if(!Object.keys(this.#codes).includes(code.toString())){
            code = 500
        }

        res.status(code).json({
            code,
            title: this.#codes[code],
            description,
            ...complement
        })


    }
}