import HttpException from "../Helpers/HttpException.js";
import bcrypt from "bcrypt";

export default class MangaController {
    model = null


    constructor(model) {
        this.model = model
    }

    async all(request, response) {
        const mangas = await this.model.all()
        return response.status(200).send(mangas.map(manga => {
            if(manga.themes !== null){
                manga.themes = manga.themes.split('|')
            }else{
                manga.themes = []
            }

            if(manga.genres !== null){
                manga.genres = manga.genres.split('|')
            }else{
                manga.genres = []
            }
            return manga
        }))
    }

    async random(request, response) {
        const mangas = await this.model.random(request?.query?.count ?? 10)
        return response.status(200).send(mangas.map(manga => {
            if(manga.themes !== null){
                manga.themes = manga.themes.split('|')
            }else{
                manga.themes = []
            }

            if(manga.genres !== null){
                manga.genres = manga.genres.split('|')
            }else{
                manga.genres = []
            }
            return manga
        }))
    }

    async top(request, response) {
        const mangas = await this.model.top(request?.query?.count ?? 10)
        return response.status(200).send(mangas.map(manga => {
            if(manga.themes !== null){
                manga.themes = manga.themes.split('|')
            }else{
                manga.themes = []
            }

            if(manga.genres !== null){
                manga.genres = manga.genres.split('|')
            }else{
                manga.genres = []
            }
            return manga
        }))
    }

    async discover(request, response) {
        const mangas = await this.model.discover(request?.query?.count ?? 10)
        return response.status(200).send(mangas.map(manga => {
            if(manga.themes !== null){
                manga.themes = manga.themes.split('|')
            }else{
                manga.themes = []
            }

            if(manga.genres !== null){
                manga.genres = manga.genres.split('|')
            }else{
                manga.genres = []
            }
            return manga
        }))
    }

    async find(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        let manga = null
        try{
            manga = (await this.model.findById(request.params.id))[0]
            if(manga.themes !== null){
                manga.themes = manga.themes.split('|')
            }else{
                manga.themes = []
            }

            if(manga.genres !== null){
                manga.genres = manga.genres.split('|')
            }else{
                manga.genres = []
            }
        }catch (error){
            return new HttpException(404, `Manga with id ${id} not found.`, response)
        }

        if(!manga){
            return new HttpException(404, `Manga with id ${id} not found.`, response)
        }

        return response.status(200).send(manga)
    }

    async store(request, response) {

        let data = JSON.parse(request.body.manga)
        let themes = []
        let genres = []

        if(typeof data === 'string'){
            data = JSON.parse(data)
        }
        const {api_id, title} = data
        if (!api_id || !title) {
            return new HttpException(422, 'Missing api_id or title in request.', response)
        }



        let mangaId, rows = null
        try {
            const {insertId, affectedRows} = await this.model.create(data)
            mangaId = insertId
            rows = affectedRows
        } catch (error) {
            console.log(error)
            if (!mangaId || rows !== 1) {
                let message = 'Could not create the manga.'
                if (error?.sqlMessage) message += ' Reason: ' + error.sqlMessage
                return new HttpException(500, message, response)
            }
        }

        if(data.themes?.length && this.model?.relationships?.themes){
            await this.model.relationships.themes.createMany(data.themes, ['id'])
            themes = (await this.model.relationships.themes.findMany(data.themes)).map(theme => theme.id)
            if(themes.length){
                await this.model.relationships.themes.connection.query(`insert ignore into mangas_themes (id_manga, id_theme) values ${themes.map(id => `(${mangaId},${id})`).join(',')};`)
            }
        }

        if(data.genres?.length && this.model?.relationships?.genres){
            await this.model.relationships.genres.createMany(data.genres)
            genres = (await this.model.relationships.genres.findMany(data.genres)).map(genre => genre.id)
            if(genres.length){
                await this.model.relationships.genres.connection.query(`insert ignore into mangas_genres (id_manga, id_genre) values ${genres.map(id => `(${mangaId},${id})`).join(',')};`)
            }
        }

        const manga = (await this.model.findById(mangaId))[0]
        if (!manga) {
            return new HttpException(404, `Manga with id ${mangaId} not found`, response)
        }

        return response.status(201).json(manga)
    }

    async update(request, response) {
        const {id} = request.params
        const {password = null, username = null} = request.body
        let newPassword = null

        if (password){
            newPassword = await new Promise((resolve, reject) => {
                bcrypt.hash(password, 10, (err, hash) => {
                    if (err) reject(err);
                    else {
                        resolve(hash);
                    }
                })
            })
        }

        const status = await this.model.update(id, {username, password: newPassword})
        if(status === -1){
            return new HttpException(422, "No data", response)
        }

        const user = (await this.model.findById(id))[0]

        if (!user){
            return new HttpException(500, '', response)
        }

        return response.status(200).json(user)
    }

    async delete(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        try{
            await this.model.delete(request.params.id)
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.sendStatus(200)
    }

    async exists(request, response){
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }

        let exists = []
        try{
            exists = await this.model.exists(id)
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.status(200).json(!!exists.length)

    }
}