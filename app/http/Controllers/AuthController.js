import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import HttpException from "../Helpers/HttpException.js";

export default class AuthController{

    async login(request, response, model, {SECRET_KEY, SECRET_EXPIRATION}){
        let { email, password } = request.body
        const user = (await model.find(email))[0]
        if (!user){
            return new HttpException(404, "User not found", response)
        }
        const auth = await new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, function(err, result) {
                if(err){
                    return new HttpException(401,'Invalid email or password', response, {auth: false})
                }
                if (result) {
                    resolve(result)
                }
            })
        })

        delete user.password

        const token = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: SECRET_EXPIRATION });
        response.status(200).send({ token, auth, user });
    }

    async logout(request, response, model){
        return response.send('Logged out!')

    }
}