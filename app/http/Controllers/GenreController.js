import HttpException from "../Helpers/HttpException.js";
import bcrypt from "bcrypt";

export default class GenreController {
    model = null

    constructor(model) {
        this.model = model
    }

    async all(request, response) {
        return response.status(200).send(await this.model.all())
    }

    async find(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        let genre = null
        try{
            genre = (await this.model.findById(request.params.id))[0]
        }catch (error){
            return new HttpException(404, `Genre with id ${id} not found.`, response)
        }

        if(!genre){
            return new HttpException(404, `Genre with id ${id} not found.`, response)
        }

        return response.status(200).send(genre)
    }

    async store(request, response, model) {

        const {name} = request.body
        if (!name) {
            return new HttpException(422, 'Missing name in request.', response)
        }

        let genreId, rows = null
        try {
            const {insertId, affectedRows} = await model.create(request.body)
            genreId = insertId
            rows = affectedRows
        } catch (error) {
            if (!genreId || rows !== 1) {
                let message = 'Could not create the genre.'
                if (error?.sqlMessage) message += ' Reason: ' + error.sqlMessage
                return new HttpException(500, message, response)
            }
        }

        const genre = (await model.find(name))[0]
        if (!genre) {
            return new HttpException(404, `Genre with id ${genreId} not found`, response)
        }

        return response.status(201).json(genre)
    }

    async delete(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        try{
            await this.model.delete(request.params.id)
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.sendStatus(200)
    }
}