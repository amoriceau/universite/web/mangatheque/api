import HttpException from "../Helpers/HttpException.js";
import bcrypt from "bcrypt";

export default class UserController {
    model = null

    constructor(model) {
        this.model = model
    }

    async all(request, response) {
        return response.status(200).send(await this.model.all())
    }

    async find(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        let user = null
        try{
            user = (await this.model.findById(request.params.id))[0]
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        if(!user){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.status(200).send(user)
    }

    async store(request, response, model) {
        let {username = 'default', email, password} = request.body
        if (!email || !password) {
            return new HttpException(422, 'Missing email or password in request.', response)
        }

        password = await new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) reject(err);
                else {
                    resolve(hash);
                }
            })
        })


        let userId, rows = null
        try {
            const {insertId, affectedRows} = await model.create({username, email, password})
            userId = insertId
            rows = affectedRows
        } catch (error) {
            if (!userId || rows !== 1) {
                let message = 'Could not create the user.'
                if (error?.sqlMessage) message += ' Reason: ' + error.sqlMessage
                return new HttpException(500, message, response)
            }
        }

        const user = (await model.findById(userId))[0]
        if (!user) {
            return new HttpException(404, `User with id ${insertId} not found`, response)
        }

        return response.status(201).json(user)
    }

    async update(request, response, model) {
        const {id} = request.params
        const {password = null, username = null} = request.body
        let newPassword = null

        if (password){
            newPassword = await new Promise((resolve, reject) => {
                bcrypt.hash(password, 10, (err, hash) => {
                    if (err) reject(err);
                    else {
                        resolve(hash);
                    }
                })
            })
        }

        const status = await model.update(id, {username, password: newPassword})
        if(status === -1){
            return new HttpException(422, "No data", response)
        }

        const user = (await model.findById(id))[0]

        if (!user){
            return new HttpException(500, '', response)
        }

        return response.status(200).json(user)
    }

    async delete(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        try{
            await this.model.delete(request.params.id)
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.sendStatus(200)
    }

    async ownVolume({params, body}, response, model){
        let {volumes} = body
        let {user_id, manga_id} = params
        if (!user_id || !user_id || !volumes) {
            return new HttpException(422, 'Missing userID, mangaID or volumes in request.', response)
        }
        if(typeof volumes === "string"){
            volumes = JSON.parse(volumes)
        }
        if(!Array.isArray(volumes)){
            volumes = [volumes]
        }
        const mangas = await model.findById(manga_id)
        let manga = null
        if(mangas.length){
            manga = mangas[0]
        }

        if(!manga){
            return new HttpException(404, `Could not find the manga with id ${manga_id}`, response)
        }

        volumes = volumes.map(volume => +volume).filter(volume => !isNaN(volume) && volume > 0 && volume <= manga.volumes)
        const userManga =  (await model.connection.queryParams('select id from user_mangas where id_manga = ? and id_user = ? limit 1;', [manga_id, user_id]))
        const userHasManga =  userManga.length === 1
        if(!userHasManga){
            return new HttpException(404, "User does not own this manga.", response)
        }

        await model.connection.queryParams("INSERT INTO user_mangas (id_manga, id_user, volumes) VALUES(?,?,?) on duplicate key update volumes = values(volumes)", [manga_id, user_id, JSON.stringify(volumes)])
        const userVolumes =  (await model.connection.queryParams('select volumes from user_mangas where id_manga = ? and id_user = ? limit 1;', [manga_id, user_id]))[0]

        response.status(200).json(userVolumes.volumes)

    }
}