import HttpException from "../Helpers/HttpException.js";
import bcrypt from "bcrypt";

export default class ThemeController {
    model = null

    constructor(model) {
        this.model = model
    }

    async all(request, response) {
        return response.status(200).send(await this.model.all())
    }

    async find(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        let theme = null
        try{
            theme = (await this.model.findById(request.params.id))[0]
        }catch (error){
            return new HttpException(404, `Theme with id ${id} not found.`, response)
        }

        if(!theme){
            return new HttpException(404, `Theme with id ${id} not found.`, response)
        }

        return response.status(200).send(theme)
    }

    async store(request, response, model) {

        const {name} = request.body
        if (!name) {
            return new HttpException(422, 'Missing name in request.', response)
        }

        let themeId, rows = null
        try {
            const {insertId, affectedRows} = await model.create(request.body)
            themeId = insertId
            rows = affectedRows
        } catch (error) {
            if (!themeId || rows !== 1) {
                let message = 'Could not create the theme.'
                if (error?.sqlMessage) message += ' Reason: ' + error.sqlMessage
                return new HttpException(500, message, response)
            }
        }

        const theme = (await model.find(name))[0]
        if (!theme) {
            return new HttpException(404, `Theme with id ${themeId} not found`, response)
        }

        return response.status(201).json(theme)
    }

    async delete(request, response) {
        const id = request.params?.id
        if(!id){
            return new HttpException(422, "Missing id parameter.", response)
        }
        try{
            await this.model.delete(request.params.id)
        }catch (error){
            return new HttpException(404, `User with id ${id} not found.`, response)
        }

        return response.sendStatus(200)
    }
}