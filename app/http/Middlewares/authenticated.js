import jwt from "jsonwebtoken";
import HttpException from "../Helpers/HttpException.js";

export default (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, process.env.SECRET_TOKEN, (err, user) => {
            if (err) {
                return new HttpException(403, 'Invalid session token', res);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};