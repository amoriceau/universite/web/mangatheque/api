import mysql from 'mysql2'
import dotenv from 'dotenv'
import chalk from "chalk";

dotenv.config()

const db = mysql.createConnection({
    host: process.env.HOST,
    port: process.env.DATABASE_PORT,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    multipleStatements: true
});

db.connect()
db.query(`
            CREATE TABLE IF NOT EXISTS \`users\`
            (
                id       int PRIMARY KEY AUTO_INCREMENT,
                email    varchar(255) NOT NULL UNIQUE,
                password varchar(255) NOT NULL,
                pseudo   varchar(255) NOT NULL
            );

            CREATE TABLE IF NOT EXISTS \`themes\`
            (
                id   int PRIMARY KEY AUTO_INCREMENT,
                name varchar(255) NOT NULL UNIQUE
            );

            CREATE TABLE IF NOT EXISTS \`genres\`
            (
                id   int PRIMARY KEY AUTO_INCREMENT,
                name varchar(255) NOT NULL UNIQUE 
            );

            CREATE TABLE IF NOT EXISTS \`mangas\`
            (
                id           int PRIMARY KEY AUTO_INCREMENT,
                api_id       varchar(255) NOT NULL UNIQUE,
                title        varchar(255) NOT NULL,
                jap_title    varchar(255),
                description  text,
                status       varchar(255),
                picture      varchar(255),
                release_date int,
                chapters     int,
                volumes     int
            );

            CREATE TABLE IF NOT EXISTS \`mangas_themes\`
            (
                id       int PRIMARY KEY AUTO_INCREMENT,
                id_theme int,
                id_manga int,
                CONSTRAINT fk_id_mt_manga FOREIGN KEY (id_manga) REFERENCES MANGAS (id) ON DELETE CASCADE,
                CONSTRAINT fk_id_mt_themes FOREIGN KEY (id_theme) REFERENCES THEMES (id),
                unique key (id_theme, id_manga)
            );

            CREATE TABLE IF NOT EXISTS \`mangas_genres\`
            (
                id       int PRIMARY KEY AUTO_INCREMENT,
                id_manga int,
                id_genre int,
                CONSTRAINT fk_id_mg_manga FOREIGN KEY (id_manga) REFERENCES MANGAS (id) ON DELETE CASCADE,
                CONSTRAINT fk_id_mg_genre FOREIGN KEY (id_genre) REFERENCES GENRES (id),
                unique key (id_genre, id_manga)
            );


            CREATE TABLE IF NOT EXISTS \`user_mangas\`
            (
                id       int PRIMARY KEY AUTO_INCREMENT,
                id_manga int,
                id_user  int,
                volumes  json,
                CONSTRAINT fk_id_um_user FOREIGN KEY (id_user) REFERENCES GENRES (id) ON DELETE CASCADE,
                CONSTRAINT fk_id_um_manga FOREIGN KEY (id_manga) REFERENCES MANGAS (id), 
                unique key (id_user, id_manga)
            
            );
        `)

console.log(chalk.greenBright.bold('DATABASE MIGRATIONS HAVE BEEN SUCCESSFULLY RUN 🍻'))