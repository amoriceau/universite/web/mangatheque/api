import mysql from 'mysql2'
import dotenv from 'dotenv'
import chalk from "chalk";

dotenv.config()

const db = mysql.createConnection({
        host: process.env.HOST,
        port: process.env.DATABASE_PORT,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        multipleStatements: true
});

db.connect()
db.query(`
            SET foreign_key_checks = 0;
            DROP TABLE IF EXISTS mangas;
            DROP TABLE IF EXISTS genres;
            DROP TABLE IF EXISTS themes;
            DROP TABLE IF EXISTS user_mangas;
            DROP TABLE IF EXISTS mangas_themes;
            DROP TABLE IF EXISTS mangas_genres;
            DROP TABLE IF EXISTS users;
            SET foreign_key_checks = 1;
        `)

console.log(chalk.hex('#F00').bold('DATABASE HAVE BEEN DROPPED SUCCESSFULLY 🔥'))