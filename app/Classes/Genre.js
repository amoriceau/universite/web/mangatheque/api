import Connector from "./Connector.js";
import HttpException from "../http/Helpers/HttpException.js";

export default class Genre extends Connector{

    static relationships = {}

    constructor() {
        super();
    }

    static setRelationShip(relation, model){
        Genre.relationships[relation] = model
        Genre[relation] = async (data, res) => {
            if(!+data.params?.id) return new HttpException(500, 'Invalid ID', res)
            const fk = `id_${relation.substring(0, relation.length-1)}`
            let mangas = []
            try {
                 mangas = await this.connection.queryParams(`SELECT ${relation}.*
                                                            FROM ${relation}
                                                                     JOIN ${relation}_genres ON ${relation}.id = ${relation}_genres.${fk}
                                                                     JOIN genres ON ${relation}_genres.id_genre = genres.id
                                                            WHERE genres.id = ?;
                `, [+data.params.id])
            }catch (err){
                return new HttpException(500, 'Could not get mangas', res, err)
            }

            return res.status(200).json(mangas)
        }
    }

    static removeRelationShip(relation){
        delete Genre.relationships[relation]
    }

    static all(keys = ['*']){
        return (async () => await this.connection.query(`select ${keys.join()} from genres;`))()
    }

    static create({name}){
        return (async () => await this.connection.queryParams(`insert ignore into genres(name) values (?);`, [name]))()
    }

    static createMany(names){
        return (async () => await this.connection.queryParams(`insert ignore into genres(name) values ${names.map(name => `('${name}')`).join(',')};`))()
    }

    static find(name, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from genres where genres.name = ? limit 1;`, [name]))()
    }

    static findMany(names, keys = ['*']){
        return (async () => await this.connection.query(`select ${keys.join(',')} from genres where genres.name in ('${names.join("','")}');`))()
    }

    static findById(id, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from genres where id = ? limit 1;`, [id]))()
    }

    static delete(id){
        return (async () => await this.connection.queryParams(`delete from genres where id = ?;`, [id]))()
    }
}