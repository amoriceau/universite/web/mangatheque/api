import Connector from "./Connector.js";
import HttpException from "../http/Helpers/HttpException.js";

export default class Manga extends Connector {

    static relationships = {}

    static attach = async (data, res) => {
        let {relation, id} = data.params
        const pivot = `mangas_${relation}`
        let {ids} = data.body
        if(!ids) return new HttpException(422, 'Missing relation id(s)',res)
        if (!Array.isArray(ids)) ids = [ids]

        const values = ids.map(relationId => `(${id},${relationId})`)
        const fk = `id_${relation.substring(0,relation.length-1)}`
        try{
            await this.connection.query(`insert ignore into ${pivot} (id_manga, ${fk}) values ${values.join(',')};`)
        }catch (err){
            return new HttpException(500, "Could not attach theme(s) to manga", res)
        }
        res.sendStatus(200)
    }

    static detach = async (data, res) => {
        let {relation, id} = data.params
        const pivot = `mangas_${relation}`
        let idToDetach = data.body.id
        if(!idToDetach) return new HttpException(422, 'Missing relation id',res)
        if (Array.isArray(idToDetach)) idToDetach = idToDetach[0]

        const fk = `id_${relation.substring(0,relation.length-1)}`
        try{
            await this.connection.queryParams(`delete
                                         from ${pivot}
                                         where id_manga = ? 
                                         and ${fk} = ?;`, [id, idToDetach])
        }catch (err){
            return new HttpException(500, "Could not detach theme from manga", res)
        }

        res.sendStatus(200)
    }

    constructor() {
        super();
    }

    static setRelationShip(relation, model) {
        Manga.relationships[relation] = model
        Manga[relation] = async (data, res) => {
            if(!+data.params?.id) return new HttpException(500, 'Invalid ID', res)
            const fk = `id_${relation.substring(0, relation.length-1)}`
            let mangas = []
            try {
                mangas = await this.connection.queryParams(`SELECT ${relation}.*
                                                            FROM ${relation}
                                                                     JOIN mangas_${relation} ON ${relation}.id = mangas_${relation}.${fk}
                                                                     JOIN mangas ON mangas_${relation}.id_manga = mangas.id
                                                            WHERE mangas.id = ?;
                `, [+data.params.id])
            }catch (err){
                return new HttpException(500, 'Could not get mangas', res, err)
            }

            return res.status(200).json(mangas)
        }

    }

    static removeRelationShip(relation) {
        delete Manga.relationships[relation]
    }

    static all(keys = ['*']) {
        return (async () => await this.connection.query(`SELECT mangas.${keys.join(', manga.')}, GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes, GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres
                                                         FROM mangas
                                                                  LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                  LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                  LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                  LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                         GROUP BY mangas.id;`))()
    }

    static random(count) {
        return (async () => await this.connection.queryParams(`SELECT mangas.*, GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes, GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres
                                                         FROM mangas
                                                                  LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                  LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                  LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                  LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                         GROUP BY mangas.id
                                                         ORDER BY RAND()
                                                         LIMIT ?
                                                        ;`, [+count]))()
    }

    static discover(count) {
        return (async () => await this.connection.queryParams(`SELECT mangas.*, GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes, GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres
                                                                FROM mangas
                                                                LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                                LEFT JOIN user_mangas ON mangas.id = user_mangas.id_manga
                                                                WHERE user_mangas.id_user IS NULL
                                                                GROUP BY mangas.id
                                                                ORDER BY RAND()
                                                         LIMIT ?
                                                        ;`, [+count]))()
    }


    static top(count) {
        return (async () => await this.connection.queryParams(`SELECT mangas.*,
                                                                      GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes,
                                                                      GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres,
                                                                      COUNT(user_mangas.id_manga)                      as followers
                                                               FROM mangas
                                                                        LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                        LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                        LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                        LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                                        LEFT JOIN user_mangas ON mangas.id = user_mangas.id_manga
                                                               GROUP BY mangas.id
                                                               ORDER BY followers DESC
                                                               LIMIT ?
        ;`, [+count]))()
    }

    static create({api_id, title, jap_title, description, picture, status, release_date, chapters, volumes = 0}) {
        return (async () => await this.connection.queryParams(`insert into mangas(api_id, title, jap_title, description,
                                                                                  status, picture, release_date,
                                                                                  chapters, volumes)
                                                               values (?, ?, ?, ?, ?, ?, ?, ?,
                                                                       ?);`, [api_id, title, jap_title, description, status, picture, release_date, chapters, volumes]))()
    }

    static find(title, keys = ['*']) {
        return (async () => await this.connection.queryParams(`SELECT mangas.${keys.join(', manga.')}, GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes, GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres
                                                               FROM mangas
                                                                        LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                        LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                        LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                        LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                               where title = ?
                                                               GROUP BY mangas.id
                                                               limit 1;`, [title]))()
    }

    static findById(id, keys = ['*']) {
        return (async () => await this.connection.queryParams(`SELECT mangas.${keys.join(', manga.')}, GROUP_CONCAT(DISTINCT themes.name SEPARATOR '|') as themes, GROUP_CONCAT(DISTINCT genres.name SEPARATOR '|') as genres
                                                               FROM mangas
                                                                LEFT JOIN mangas_themes ON mangas.id = mangas_themes.id_manga
                                                                LEFT JOIN themes ON mangas_themes.id_theme = themes.id
                                                                LEFT JOIN mangas_genres ON mangas.id = mangas_genres.id_manga
                                                                LEFT JOIN genres ON mangas_genres.id_genre = genres.id
                                                               where mangas.id = ? or mangas.api_id = ?
                                                               GROUP BY mangas.id
                                                               limit 1;`, [id, id]))()
    }

    static exists(id){
        return (async () => await this.connection.queryParams(`SELECT mangas.id, mangas.api_id
                                                               FROM mangas
                                                               where mangas.api_id = ?
                                                               limit 1;`, [id]))()
    }

    static delete(id) {
        return (async () => await this.connection.queryParams(`delete
                                                               from mangas
                                                               where id = ?;`, [id]))()
    }

    static update(id, {password, username}) {
        const keys = []
        const data = []

        if (password) {
            keys.push('password')
            data.push(password)
        }
        if (username) {
            keys.push('pseudo')
            data.push(username)
        }

        if (!data.length) {
            return -1
        }

        const query = (keys.map((key, index) => `${key}=? ${index < data.length - 1 ? ',' : ''}`)).join('')

        return (async () => await this.connection.queryParams(`update mangas
                                                               set ${query}
                                                               where id = ?;`, [...data, id]))()

    }
}