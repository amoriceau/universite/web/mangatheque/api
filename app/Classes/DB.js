import mysql from 'mysql2'

export default class DB {
    #connection = null;

    constructor(options, connect = true) {
        this.#connection = mysql.createConnection(options);

        connect && this.#connection.connect(() => {
            console.log(`Super tu es connecté a la base ${options.database}`)
        });

        return this
    }

    async query(query){
        return await this.#connection.promise().query(query).then(([rows,fields]) => {
            return rows
        })
    }

    async queryParams(query, params = []){
        return await this.#connection.promise().query(query, params).then(([rows,fields]) => {
            return rows
        })
    }
}