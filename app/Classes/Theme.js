import Connector from "./Connector.js";
import HttpException from "../http/Helpers/HttpException.js";

export default class Theme extends Connector{

    static relationships = {}
    constructor() {
        super();
    }

    static setRelationShip(relation, model){
        Theme.relationships[relation] = model
        Theme[relation] = async (data, res) => {
            if(!+data.params?.id) return new HttpException(500, 'Invalid ID', res)
            const fk = `id_${relation.substring(0, relation.length-1)}`
            let mangas = []
            try {
                mangas = await this.connection.query(`SELECT ${relation}.*
                                                            FROM ${relation}
                                                                     JOIN ${relation}_themes ON ${relation}.id = ${relation}_themes.${fk}
                                                                     JOIN themes ON ${relation}_themes.id_theme = themes.id
                                                            WHERE themes.id = ?;
                `, [+data.params.id])
            }catch (err){
                return new HttpException(500, 'Could not get mangas', res, err)
            }

            return res.status(200).json(mangas)
        }
    }

    static removeRelationShip(relation){
        delete Theme.relationships[relation]
    }

    static all(keys = ['*']){
        return (async () => await this.connection.query(`select ${keys.join()} from themes;`))()
    }

    static create({name}){
        return (async () => await this.connection.queryParams(`insert ignore into themes(name) values (?) on duplicate key update name = ?;`, [name, name]))()
    }

    static createMany(names){
        return (async () => await this.connection.queryParams(`insert ignore into themes(name) values ${names.map(name => `("${name}")`).join(',')};`))()
    }

    static find(name, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from themes where themes.name = ? limit 1;`, [name]))()
    }

    static findMany(names, keys = ['*']){
        return (async () => await this.connection.query(`select ${keys.join(',')} from themes where themes.name in ('${names.join("','")}');`))()
    }

    static findById(id, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from themes where id = ? limit 1;`, [id]))()
    }

    static delete(id){
        return (async () => await this.connection.queryParams(`delete from themes where id = ?;`, [id]))()
    }
}