import Connector from "./Connector.js";
import HttpException from "../http/Helpers/HttpException.js";

export default class User extends Connector{

    static relationships = {}

    static attach = async (data, res) => {
        let {relation, id} = data.params
        const pivot = `user_${relation}`
        let {ids} = data.body
        if(!ids) return new HttpException(422, 'Missing relation id(s)',res)
        if (!Array.isArray(ids)) ids = [ids]

        const values = ids.map(relationId => `(${id},${relationId})`)
        const fk = `id_${relation.substring(0,relation.length-1)}`
        try{
            await this.connection.query(`insert ignore into ${pivot} (id_user, ${fk}) values ${values.join(',')};`)
        }catch (err){
            return new HttpException(500, "Could not attach manga(s) to user", res)
        }
        res.sendStatus(200)
    }

    static detach = async (data, res) => {
        let {relation, id} = data.params
        const pivot = `user_${relation}`
        let idToDetach = data.body.id
        if(!idToDetach) return new HttpException(422, 'Missing relation id',res)
        if (Array.isArray(idToDetach)) idToDetach = idToDetach[0]

        const fk = `id_${relation.substring(0,relation.length-1)}`
        try{
            await this.connection.queryParams(`delete
                                         from ${pivot}
                                         where id_user = ? 
                                         and ${fk} = ?;`, [id, idToDetach])
        }catch (err){
            return new HttpException(500, "Could not manga theme from user", res)
        }

        res.sendStatus(200)
    }
    constructor() {
        super();
    }

    static setRelationShip(relation, model) {
        User.relationships[relation] = model
        User[relation] = async (data, res) => {
            if(!+data.params?.id) return new HttpException(500, 'Invalid ID', res)
            const fk = `id_${relation.substring(0, relation.length-1)}`

            let mangas = []
            try {
                mangas = await this.connection.queryParams(`SELECT ${relation}.*, user_${relation}.volumes
                                                            FROM ${relation}
                                                                     JOIN user_${relation} ON ${relation}.id = user_${relation}.${fk}
                                                                     JOIN users ON user_${relation}.id_user = users.id
                                                            WHERE users.id = ?;
                `, [+data.params.id])
            }catch (err){
                return new HttpException(500, 'Could not get mangas', res, err)
            }

            return res.status(200).json(mangas)
        }

    }

    static removeRelationShip(relation) {
        delete Manga.relationships[relation]
    }

    static all(keys = ['*']){
        return (async () => await this.connection.query(`select ${keys.join()} from users;`))()
    }

    static create({email, password, username}){
        return (async () => await this.connection.queryParams(`insert into users(email, password, pseudo) values (? , ?, ?);`, [email, password, username]))()
    }

    static find(email, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from users where email = ? limit 1;`, [email]))()
    }

    static findById(id, keys = ['*']){
        return (async () => await this.connection.queryParams(`select ${keys.join()} from users where id = ? limit 1;`, [id]))()
    }

    static delete(id){
        return (async () => await this.connection.queryParams(`delete from users where id = ?;`, [id]))()
    }

    static update(id, {password, username}){
        const keys = []
        const data = []

        if(password) {
            keys.push('password')
            data.push(password)
        }
        if (username) {
            keys.push('pseudo')
            data.push(username)
        }

        if (!data.length){
            return -1
        }

        const query = (keys.map((key, index) => `${key}=? ${index < data.length-1 ? ',':''}`)).join('')

        return (async () => await this.connection.queryParams(`update users set ${query} where id = ?;`, [...data, id]))()

    }
}